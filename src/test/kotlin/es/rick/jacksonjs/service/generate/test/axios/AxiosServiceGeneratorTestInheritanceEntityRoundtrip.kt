package es.rick.jacksonjs.service.generate.test.axios

import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.entity.generator.EntityFileGenerator
import es.rick.jacksonjs.entity.test.entity.Cat
import es.rick.jacksonjs.entity.test.entity.Dog
import es.rick.jacksonjs.resources.ResourcePaths
import es.rick.jacksonjs.service.JaxRsServiceClassReader
import es.rick.jacksonjs.service.SpringWebServiceClassReader
import es.rick.jacksonjs.service.generate.test.service.ServiceGenerationTestClassEntityInheritanceRoundtrip
import es.rick.jacksonjs.service.generate.test.service.ServiceGenerationTestClassEntityInheritanceRoundtripImpl
import es.rick.jacksonjs.service.generator.axios.AxiosServiceFileGenerator
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.Bean
import java.time.LocalDate
import kotlin.reflect.full.findAnnotations

@SpringBootTest(
    classes = [AxiosServiceGeneratorTestInheritanceEntityRoundtrip.TestApp::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
class AxiosServiceGeneratorTestInheritanceEntityRoundtrip {
    @LocalServerPort
    var serverPort = 0

    @Autowired
    lateinit var restServiceImpl: ServiceGenerationTestClassEntityInheritanceRoundtripImpl

    @OptIn(ExperimentalStdlibApi::class)
    @Test
    fun inheritanceEntityRoundtrip() {
        val serviceClass =
            SpringWebServiceClassReader().readServiceClass(ServiceGenerationTestClassEntityInheritanceRoundtrip::class)
        val serviceFileGenerator = AxiosServiceFileGenerator()
        val tsEntityFiles =
            ServiceGenerationTestClassEntityInheritanceRoundtrip::class.findAnnotations(TsEntityFile::class)

        val tsEntityResults = EntityFileGenerator().processEntityFiles(tsEntityFiles.toTypedArray())
        ResourcePaths.writeFileInOutputDir("entities.ts", tsEntityResults.first().content)

        val stub = serviceFileGenerator.generateServiceStub(serviceClass, tsEntityFiles)
        ResourcePaths.writeFileInOutputDir("service.ts", stub)
        val serviceSimpleName = ServiceGenerationTestClassEntityInheritanceRoundtrip::class.simpleName
        val tsCalls = """
            import {$serviceSimpleName} from './service';
            
            const service = new $serviceSimpleName(() => "http://localhost:$serverPort");
            
            async function main() {
                const pets = await service.getPets();
                await service.setPets(pets.data);
            }
            main().then(() => console.info("finished"))
        """.trimIndent()
        val petsBefore = restServiceImpl.getPets()
        "index.ts".let {
            ResourcePaths.writeFileInOutputDir(it, tsCalls)
            ResourcePaths.runNpmInOutputDirAndPrintOutput()
        }
        val petsAfter = restServiceImpl.getPets()
        Assertions.assertEquals(petsBefore, petsAfter)
    }

    @SpringBootApplication
    class TestApp : SpringBootServletInitializer() {
        @Bean
        fun restServiceImpl() = ServiceGenerationTestClassEntityInheritanceRoundtripImpl(
            listOf(
                Cat(
                    name = "Minky",
                    birthDate = LocalDate.of(2050, 10, 9),
                    lifesRemaining = 7
                ),
                Dog(
                    name = "Sparky",
                    birthDate = LocalDate.of(2049, 11, 13),
                    postmenBitten = 7
                )
            )
        )
    }
}