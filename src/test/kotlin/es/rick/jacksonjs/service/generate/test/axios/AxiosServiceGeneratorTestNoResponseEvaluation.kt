package es.rick.jacksonjs.service.generate.test.axios

import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.entity.generator.EntityFileGenerator
import es.rick.jacksonjs.resources.ResourcePaths
import es.rick.jacksonjs.resources.ResourcePaths.Companion.runNpmInOutputDirAndPrintOutput
import es.rick.jacksonjs.resources.ResourcePaths.Companion.writeFileInOutputDir
import es.rick.jacksonjs.service.JaxRsServiceClassReader
import es.rick.jacksonjs.service.generate.test.service.ServiceGenerationTestClassQueryPathHeaderPrimitivesImpl
import es.rick.jacksonjs.service.generator.axios.AxiosServiceFileGenerator
import es.rick.jacksonjs.service.read.test.JaxRsServiceClassReaderTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.Bean
import java.util.*
import kotlin.reflect.full.findAnnotations

@SpringBootTest(
    classes = [AxiosServiceGeneratorTestNoResponseEvaluation.TestApp::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
class AxiosServiceGeneratorTestNoResponseEvaluation {
    @LocalServerPort
    var serverPort = 0

    @Autowired
    lateinit var restServiceImpl: ServiceGenerationTestClassQueryPathHeaderPrimitivesImpl

    @OptIn(ExperimentalStdlibApi::class)
    @Test
    fun simple() {
        val serviceClass =
            JaxRsServiceClassReader().readServiceClass(JaxRsServiceClassReaderTest.A::class)
        val serviceFileGenerator = AxiosServiceFileGenerator()
        val tsEntityFiles =
            JaxRsServiceClassReaderTest.A::class.findAnnotations(TsEntityFile::class)

        val tsEntityResults = EntityFileGenerator().processEntityFiles(tsEntityFiles.toTypedArray())
        writeFileInOutputDir("entities.ts", tsEntityResults.first().content)

        val stub = serviceFileGenerator.generateServiceStub(serviceClass, tsEntityFiles)
        writeFileInOutputDir("service.ts", stub)
        val stringParam = UUID.randomUUID().toString()
        val intParamOptional = (Math.random() * 100000).toInt()
        val pathParam = UUID.randomUUID().toString()
        val headerParam = UUID.randomUUID().toString()
        val tsCalls = """
            import {A} from './service';
            
            const service = new A(() => "http://localhost:$serverPort");
            
            async function main() {
                await service.basicQueryParams("$stringParam", $intParamOptional);
                await service.pathParam("$pathParam");
                await service.headerParam("$headerParam");
            }
            main().then(() => console.info("finished"))
        """.trimIndent()
        "index.ts".let {
            writeFileInOutputDir(it, tsCalls)
            runNpmInOutputDirAndPrintOutput()
        }
        assertEquals(stringParam, restServiceImpl.stringParam)
        assertEquals(intParamOptional, restServiceImpl.intParam)
        assertEquals(pathParam, restServiceImpl.pathParam)
        assertEquals(headerParam, restServiceImpl.headerParam)
    }

    @SpringBootApplication
    class TestApp : SpringBootServletInitializer() {
        @Bean
        fun restServiceImpl() = ServiceGenerationTestClassQueryPathHeaderPrimitivesImpl()
    }
}