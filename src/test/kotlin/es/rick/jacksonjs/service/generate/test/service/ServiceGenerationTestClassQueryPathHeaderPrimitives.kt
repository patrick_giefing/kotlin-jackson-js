package es.rick.jacksonjs.service.generate.test.service

import es.rick.jacksonjs.service.read.test.JaxRsServiceClassReaderTest
import org.springframework.web.bind.annotation.*

@RequestMapping("/query-path-header-primitives")
interface ServiceGenerationTestClassQueryPathHeaderPrimitives {
    @GetMapping("/basic-query-params/")
    fun basicQueryParams(
        @RequestParam("stringParam") stringParam: String,
        @RequestParam("intParamOptional") intParam: Int?
    ): JaxRsServiceClassReaderTest.EntityA

    @RequestMapping("/{pathParam}/", method = [RequestMethod.GET])
    fun pathParam(@PathVariable("pathParam") pathParam: String): Int

    @GetMapping("/header/")
    fun headerParam(@RequestHeader("headerParam") headerParam: String)
}

@RestController
class ServiceGenerationTestClassQueryPathHeaderPrimitivesImpl : ServiceGenerationTestClassQueryPathHeaderPrimitives {
    var stringParam: String = ""
    var intParam: Int? = null
    var pathParam: String = ""
    var headerParam: String = ""

    override fun basicQueryParams(stringParam: String, intParam: Int?): JaxRsServiceClassReaderTest.EntityA {
        this.stringParam = stringParam
        this.intParam = intParam
        return JaxRsServiceClassReaderTest.EntityA(
            n = intParam ?: -1,
            s = stringParam
        )
    }

    override fun pathParam(pathParam: String): Int {
        this.pathParam = pathParam
        return pathParam.length
    }

    override fun headerParam(headerParam: String) {
        this.headerParam = headerParam
    }
}
