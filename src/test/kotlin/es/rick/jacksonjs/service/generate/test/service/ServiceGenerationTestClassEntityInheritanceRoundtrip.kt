package es.rick.jacksonjs.service.generate.test.service

import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.entity.test.entity.Cat
import es.rick.jacksonjs.entity.test.entity.Dog
import es.rick.jacksonjs.entity.test.entity.Pet
import org.springframework.web.bind.annotation.*

@TsEntityFile(
    fileName = "./entities.ts",
    entityTypes = [Pet::class, Cat::class, Dog::class]
)
@RequestMapping("/entity-inheritance-roundtrip/pets")
interface ServiceGenerationTestClassEntityInheritanceRoundtrip {
    @GetMapping
    fun getPets(): List<Pet>

    @PutMapping
    fun setPets(@RequestBody pets: List<Pet>)
}

@RestController
class ServiceGenerationTestClassEntityInheritanceRoundtripImpl(private var pets: List<Pet>) :
    ServiceGenerationTestClassEntityInheritanceRoundtrip {
    override fun getPets(): List<Pet> {
        return pets
    }

    override fun setPets(pets: List<Pet>) {
        this.pets = pets
    }
}