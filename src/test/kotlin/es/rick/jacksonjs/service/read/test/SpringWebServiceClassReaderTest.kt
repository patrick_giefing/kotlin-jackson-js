package es.rick.jacksonjs.service.read.test

import es.rick.jacksonjs.service.SpringWebServiceClassReader
import es.rick.jacksonjs.service.generate.test.service.ServiceGenerationTestClassQueryPathHeaderPrimitives
import es.rick.jacksonjs.service.generate.test.service.ServiceGenerationTestClassQueryPathHeaderPrimitivesImpl
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.Bean

@SpringBootTest(
    classes = [SpringWebServiceClassReaderTest.TestApp::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
class SpringWebServiceClassReaderTest {
    @Test
    fun a() {
        val serviceClass =
            SpringWebServiceClassReader().readServiceClass(ServiceGenerationTestClassQueryPathHeaderPrimitives::class)
        print(serviceClass)
        Thread.sleep(60000000)
        // TODO asserts
    }


    @SpringBootApplication
    class TestApp : SpringBootServletInitializer() {
        @Bean
        fun a() = ServiceGenerationTestClassQueryPathHeaderPrimitivesImpl()
    }
}