package es.rick.jacksonjs.service.read.test

import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.service.JaxRsServiceClassReader
import org.junit.jupiter.api.Test
import javax.ws.rs.*

class JaxRsServiceClassReaderTest {
    @Test
    fun a() {
        val serviceClass = JaxRsServiceClassReader().readServiceClass(A::class)
        print(serviceClass)
        // TODO asserts
    }

    data class EntityA(
        var n: Int,
        var s: String
    )


    @TsEntityFile(entityTypes = [EntityA::class], fileName = "entities", entityFileImports = [])
    @Path("/root/")
    interface A {
        @GET
        @Path("/basic-query-params/")
        fun basicQueryParams(
            @QueryParam("stringParam") stringParam: String,
            @QueryParam("intParamOptional") intParam: Int?
        ): String

        @GET
        @Path("/{pathParam}/")
        fun pathParam(@PathParam("pathParam") pathParam: String): Int

        @GET
        @Path("/header/")
        fun headerParam(@HeaderParam("headerParam") headerParam: String)
    }
}