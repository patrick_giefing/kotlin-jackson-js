package es.rick.jacksonjs.service.read.test

import es.rick.jacksonjs.service.JaxRsServiceClassReader
import es.rick.jacksonjs.service.SpringWebServiceClassReader
import es.rick.jacksonjs.service.generate.test.service.ServiceGenerationTestClassQueryPathHeaderPrimitives
import es.rick.jacksonjs.service.generator.ServiceFileGenerator
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.web.bind.annotation.*
import javax.ws.rs.*
import kotlin.reflect.KClass
import kotlin.reflect.full.starProjectedType

class MultipleServiceClassReaderTest {
    @Test
    fun a() {
        val jaxRsServiceClass =
            JaxRsServiceClassReader().readServiceClass(JaxRsServiceClassReaderTest.A::class)
        val springWebServiceClass =
            SpringWebServiceClassReader().readServiceClass(ServiceGenerationTestClassQueryPathHeaderPrimitives::class)
        Assertions.assertEquals(jaxRsServiceClass, springWebServiceClass)

        val usedClassesExpected = setOf<KClass<*>>(Unit::class, A::class, String::class, Int::class)
        val usedClasses = ServiceFileGenerator.getUsedClasses(A::class.starProjectedType)
        Assertions.assertEquals(usedClassesExpected, usedClasses)
    }

    @Path("/root/")
    @RequestMapping("/root/")
    interface A {
        @GET
        @Path("/basic-query-params/")
        @GetMapping("/basic-query-params/")
        fun basicQueryParams(
            @QueryParam("stringParam") @RequestParam("stringParam") stringParam: String,
            @QueryParam("intParamOptional") @RequestParam("intParamOptional") intParam: Int?
        )

        @GET
        @Path("/{pathParam}/")
        @RequestMapping("/{pathParam}/", method = [RequestMethod.GET])
        fun pathParam(@PathParam("pathParam") @PathVariable("pathParam") pathParam: String)

        @GET
        @Path("/header/")
        @GetMapping("/header/")
        fun headerParam(@HeaderParam("headerParam") @RequestHeader("headerParam") headerParam: String)
    }
}