package es.rick.jacksonjs.type.test

import com.fasterxml.jackson.annotation.JsonSubTypes
import es.rick.jacksonjs.entity.generator.EntityFileGenerator
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.reflect.KClass
import kotlin.reflect.full.starProjectedType

@OptIn(ExperimentalStdlibApi::class)
class TypeImportUtilsTest {
    @Test
    fun jsonSubTypesPrimitivesAndObjects() {
        val usedClassesExpected = setOf<KClass<*>>(
            A::class,
            B::class,
            D::class,
            Type::class,
            Super::class,
            SubTypeA::class,
            SubTypeB::class,
            String::class,
            List::class,
            Map::class
        )
        val usedClasses = EntityFileGenerator.getUsedClasses(A::class)
        Assertions.assertEquals(usedClassesExpected, usedClasses)
    }

    abstract class Super<T> where T : Any {
        var cSet: Set<C> = setOf()
    }

    @JsonSubTypes(
        JsonSubTypes.Type(value = SubTypeA::class, name = "a"),
        JsonSubTypes.Type(value = SubTypeB::class)
    )
    class A : Super<Type>() {
        var type: Type? = null
        var bList: List<B> = listOf()
        var dMap: Map<String, D> = mapOf()
    }

    class B {}
    class C {}
    class D {}
    class Type {}
    class SubTypeA {}
    class SubTypeB {}
}