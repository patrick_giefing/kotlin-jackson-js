package es.rick.jacksonjs.entity.test

import es.rick.jacksonjs.entity.generator.TypeToJacksonJsJsonClassTypeConverter
import es.rick.jacksonjs.entity.generator.TypeToTsDeclarationConverter
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import kotlin.reflect.jvm.kotlinProperty

class TypeConversionTest {
    @Nested
    inner class StringConversionTest {
        @Test
        fun string() {
            assertTsTypesEqual("string", "string", "[String]")
        }

        @Test
        fun stringArray() {
            assertTsTypesEqual("stringArray", "string[]", "[Array, [String]]")
        }

        @Test
        fun stringList() {
            assertTsTypesEqual("stringList", "string[]", "[Array, [String]]")
        }

        @Test
        fun stringSet() {
            assertTsTypesEqual("stringSet", "string[]", "[Array, [String]]")
        }

        @Test
        fun stringCollection() {
            assertTsTypesEqual("stringCollection", "string[]", "[Array, [String]]")
        }

        @Test
        fun stringIterable() {
            assertTsTypesEqual("stringIterable", "string[]", "[Array, [String]]")
        }
    }

    @Nested
    inner class NumberConversionTest {
        @Test
        fun integer() {
            assertTsTypesEqual("integer", "number", "[Number]")
        }

        @Test
        fun floatArray() {
            assertTsTypesEqual("floatArray", "number[]", "[Array, [Number]]")
        }

        @Test
        fun doubleList() {
            assertTsTypesEqual("doubleList", "number[]", "[Array, [Number]]")
        }

        @Test
        fun longSet() {
            assertTsTypesEqual("longSet", "number[]", "[Array, [Number]]")
        }
    }

    @Nested
    inner class BooleanConversionTest {
        @Test
        fun boolean() {
            assertTsTypesEqual("boolean", "Boolean", "[Boolean]")
        }

        @Test
        fun booleanArray() {
            assertTsTypesEqual("booleanArray", "boolean[]", "[Array, [Boolean]]")
        }
    }

    data class EntityTypes(
        val string: String = "a",
        val stringArray: Array<String> = arrayOf("a", "b", "c"),
        val stringList: List<String> = listOf("a", "b", "c"),
        val stringSet: Set<String> = setOf("a", "b", "c"),
        val stringCollection: Set<String> = setOf("a", "b", "c"),
        val stringIterable: Set<String> = setOf("a", "b", "c"),

        val integer: Int = 1,
        val floatArray: Array<Float> = arrayOf(1.1f, 2.2f, 3.3f),
        val doubleList: List<Double> = listOf(1.1, 2.2, 3.3),
        val longSet: Set<Long> = setOf(1, 2, 3),

        val boolean: Boolean = true,
        val booleanArray: Array<Boolean> = arrayOf(true, false)
    )

    fun assertTsTypesEqual(fieldName: String, tsTypeExpected: String, tsTypeDeclarationExpected: String) {
        val field = EntityTypes::class.java.declaredFields.first { it.name == fieldName }
        val type = field.kotlinProperty!!.returnType
        val tsType = TypeToTsDeclarationConverter().convert(type)
        val tsTypeDeclaration = TypeToJacksonJsJsonClassTypeConverter().convert(type)
        assertEquals(tsTypeExpected, tsType)
        assertEquals(tsTypeDeclarationExpected, tsTypeDeclaration)
    }
}