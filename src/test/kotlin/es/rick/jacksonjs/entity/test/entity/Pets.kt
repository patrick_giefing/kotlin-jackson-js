package es.rick.jacksonjs.entity.test.entity

import com.fasterxml.jackson.annotation.*
import java.time.LocalDate

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY, property = "type"
)
@JsonSubTypes(
    JsonSubTypes.Type(value = Cat::class, name = "cat"),
    JsonSubTypes.Type(value = Dog::class, name = "dog")
)
abstract class Pet(
    open var name: String,
    @field:JsonFormat(
        shape = JsonFormat.Shape.STRING,
        pattern = "yyyy-MM-dd"
    )
    open var birthDate: LocalDate
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Pet) return false

        if (name != other.name) return false
        if (birthDate != other.birthDate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + birthDate.hashCode()
        return result
    }
}

@JsonTypeName("cat")
class Cat(
    override var name: String,
    override var birthDate: LocalDate,
    var lifesRemaining: Int = 9
) : Pet(name, birthDate) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Cat

        if (name != other.name) return false
        if (birthDate != other.birthDate) return false
        if (lifesRemaining != other.lifesRemaining) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + birthDate.hashCode()
        result = 31 * result + lifesRemaining
        return result
    }
}

@JsonTypeName("dog")
class Dog(
    override var name: String,
    override var birthDate: LocalDate,
    //@JsonProperty("postmen-bitten")
    //@get:JsonProperty("postmen-bitten")
    @field:JsonProperty("postmen-bitten")
    //@property:JsonProperty("postmen-bitten")
    var postmenBitten: Int = 0
) : Pet(name, birthDate) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Dog

        if (name != other.name) return false
        if (birthDate != other.birthDate) return false
        if (postmenBitten != other.postmenBitten) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + birthDate.hashCode()
        result = 31 * result + postmenBitten
        return result
    }
}

class PetOwner(
    val name: String,
    val pets: Array<Pet>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PetOwner

        if (name != other.name) return false
        if (!pets.contentEquals(other.pets)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + pets.contentHashCode()
        return result
    }
}