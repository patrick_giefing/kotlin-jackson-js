package es.rick.jacksonjs.entity.test.entity

data class OneStringWithoutAnnotationsTestEntity(
    var stringField: String = ""
)