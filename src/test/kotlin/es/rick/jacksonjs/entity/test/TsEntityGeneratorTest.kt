package es.rick.jacksonjs.entity.test

import com.fasterxml.jackson.databind.ObjectMapper
import es.rick.jacksonjs.entity.annotation.TsPackage
import es.rick.jacksonjs.entity.generator.EntityFileGenerator
import es.rick.jacksonjs.entity.generator.TestFileGenerator
import es.rick.jacksonjs.entity.generator.TsFileResult
import es.rick.jacksonjs.resources.ResourcePaths.Companion.fileInOutputDir
import es.rick.jacksonjs.resources.ResourcePaths.Companion.outputDir
import es.rick.jacksonjs.resources.ResourcePaths.Companion.runNpmInOutputDirAndPrintOutput
import es.rick.jacksonjs.resources.ResourcePaths.Companion.runTsFileInOutputDirAndPrintOutput
import es.rick.jacksonjs.resources.ResourcePaths.Companion.runTscInOutputDirAndPrintOutput
import es.rick.jacksonjs.resources.ResourcePaths.Companion.writeFileInOutputDir
import org.junit.jupiter.api.Assertions
import java.nio.file.Path
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.full.findAnnotations
import kotlin.reflect.jvm.jvmErasure


abstract class TsEntityGeneratorTest {
    @OptIn(ExperimentalStdlibApi::class)
    fun getAnnotationOrFail(clazz: KClass<*>): TsPackage {
        return clazz.findAnnotations(TsPackage::class).firstOrNull()
            ?: error("Annotation ${TsPackage::class.qualifiedName} not found on class ${clazz.qualifiedName}")
    }

    fun testTsPackageAnnotatedClass(annotatedClass: KClass<*>, inputObject: Any, entityTestClass: KType, entitiesTestFile: String) {
        val entityFiles = getAnnotationOrFail(annotatedClass).entityFiles
        val generatorResult = EntityFileGenerator().processEntityFiles(entityFiles)
        write(generatorResult)
        val inputFile = fileInOutputDir("input.json")
        val outputFile = fileInOutputDir( "output.json")
        outputFile.delete()
        val inputJsonContent = ObjectMapper().writeValueAsString(inputObject)
        writeFileInOutputDir("input.json", inputJsonContent)
        val testFileContent =
            TestFileGenerator().generate(inputFile.absolutePath, outputFile.absolutePath, entityTestClass, entitiesTestFile)
        writeFileInOutputDir("index.ts", testFileContent)
        //deleteEntityFiles(entityFiles)
        //runTscInOutputDirAndPrintOutput()
        //runTsFileInOutputDirAndPrintOutput("index.js")
        runNpmInOutputDirAndPrintOutput()
        val resultJson = outputFile.readText()
        val outputObject = ObjectMapper().readValue(resultJson, entityTestClass.jvmErasure.javaObjectType)
        Assertions.assertEquals(inputObject, outputObject)
    }

    private fun write(results: Array<TsFileResult>) {
        results.forEach { result ->
            val tsFile = Path.of(outputDir.absolutePath, result.fileName).toFile()
            tsFile.writeText(result.content)
        }
    }
}