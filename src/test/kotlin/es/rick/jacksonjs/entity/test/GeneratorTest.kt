package es.rick.jacksonjs.entity.test

import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.entity.annotation.TsPackage
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.reflect.full.starProjectedType

class GeneratorTest : TsEntityGeneratorTest() {
    @Test
    fun generate() {
        testTsPackageAnnotatedClass(Structure::class, OneStringWithoutAnnotationsTestEntity(stringField = "s"), OneStringWithoutAnnotationsTestEntity::class.starProjectedType, "entities.ts")
    }

    private fun deleteEntityFiles(entityFiles: Array<TsEntityFile>) {
        entityFiles.forEach { File(it.fileName).delete() }
    }

    @TsPackage(
        entityFiles = [
            TsEntityFile(
                fileName = "./entities.ts",
                entityTypes = [OneStringWithoutAnnotationsTestEntity::class]
            )
        ]
    )
    class Structure {}

    data class OneStringWithoutAnnotationsTestEntity(
        var stringField: String = ""
    )
}