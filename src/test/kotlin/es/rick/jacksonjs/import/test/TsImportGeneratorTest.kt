package es.rick.jacksonjs.import.test

import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.import.TsImportGenerator
import es.rick.jacksonjs.service.generator.ServiceFileGenerator.Companion.NL
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.reflect.full.findAnnotations

class TsImportGeneratorTest {
    @Test
    fun generateImportString() {
        val classImports = mapOf(
            "/entities/1.ts" to setOf(String::class, Int::class),
            "/entities/2.ts" to setOf(Long::class, Double::class)
        )
        val expectedImportString =
            "import {String, Int} from /entities/1.ts$NL" +
                    "import {Long, Double} from /entities/2.ts"
        val actualImportString = TsImportGenerator.generateImportString(classImports)
        Assertions.assertEquals(expectedImportString, actualImportString)
    }

    @OptIn(ExperimentalStdlibApi::class)
    @Test
    fun findImports() {
        val tsEntityFile = A::class.findAnnotations(TsEntityFile::class).first()
        val importsExpected = mapOf("file2.ts" to listOf(Int::class))
        val importsActual = TsImportGenerator.findImports(
            setOf(String::class),
            setOf(String::class, Int::class),
            tsEntityFile.entityFileImports.toList()
        )
        Assertions.assertEquals(importsExpected, importsActual)
    }

    @TsEntityFile(
        fileName = "",
        entityTypes = [],
        entityFileImports = [
            TsEntityFile(
                "file1.ts",
                arrayOf(Double::class),
                arrayOf()
            ),
            TsEntityFile(
                "file2.ts",
                arrayOf(Int::class),
                arrayOf()
            )
        ]
    )
    class A {}
}