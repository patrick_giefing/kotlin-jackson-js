package es.rick.jacksonjs.tspackage.test

import es.rick.jacksonjs.entity.annotation.TsPackage
import es.rick.jacksonjs.entity.generator.EntityFileGenerator
import es.rick.jacksonjs.tspackage.test.tspackage.EntitiesDependenciesInDifferentFiles
import org.junit.jupiter.api.Test

class EntityPackageTest {
    @Test
    fun testPackage() {
        val ann = EntitiesDependenciesInDifferentFiles::class.annotations.firstOrNull()
            ?: error("no annotation found")
        println(ann)
        val tsPackage = EntitiesDependenciesInDifferentFiles::class.java.getAnnotation(TsPackage::class.java)
        EntityFileGenerator().processEntityFiles(tsPackage.entityFiles)
    }
}