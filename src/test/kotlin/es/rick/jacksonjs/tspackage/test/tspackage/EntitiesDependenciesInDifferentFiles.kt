package es.rick.jacksonjs.tspackage.test.tspackage

import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.entity.annotation.TsPackage
import es.rick.jacksonjs.entity.test.entity.Cat
import es.rick.jacksonjs.entity.test.entity.Dog
import es.rick.jacksonjs.entity.test.entity.Pet
import es.rick.jacksonjs.entity.test.entity.PetOwner

@TsPackage(
    entityFiles = [
        TsEntityFile(
            "pets.ts", [
                Pet::class, Cat::class, Dog::class
            ]
        ),
        TsEntityFile(
            "owner.ts", [
                PetOwner::class
            ]
        )
    ]
)
class EntitiesDependenciesInDifferentFiles {
}