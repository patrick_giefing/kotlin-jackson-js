package es.rick.jacksonjs.resources

import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.nio.file.Path

class ResourcePaths {
    companion object {
        // TODO dynamic path
        val outputDir = File("C:\\dev\\projects\\kotlin-jackson-js\\src\\main\\resources\\")

        fun runTscInOutputDir() =
            Runtime.getRuntime().exec(
                arrayOf("C:\\dev\\projects\\angular-test\\node_modules\\.bin\\tsc.cmd", "-p", "./"),
                null,
                outputDir
            )

        fun runTscInOutputDirAndPrintOutput() {
            val p = runTscInOutputDir()
            printProcessOutput(p)
        }

        fun runTsFileInOutputDir(file: String) =
            Runtime.getRuntime().exec(arrayOf("node", file), null, outputDir)

        fun runTsFileInOutputDirAndPrintOutput(file: String) {
            val p = runTsFileInOutputDir(file)
            printProcessOutput(p)
        }

        fun runNpmInOutputDir() =
            Runtime.getRuntime().exec(arrayOf("cmd", "/c", "start", "npm-start.bat"), null, outputDir)

        fun runNpmInOutputDirAndPrintOutput() {
            val p = runNpmInOutputDir()
            printProcessOutput(p)
        }


        fun printProcessOutput(proc: Process) {
            val stdInput = BufferedReader(InputStreamReader(proc.inputStream))
            val stdError = BufferedReader(InputStreamReader(proc.errorStream))

            println("Here is the standard output of the command:\n")
            var s: String?
            while (stdInput.readLine().also { s = it } != null) {
                println(s)
            }

            println("Here is the standard error of the command (if any):\n")
            while (stdError.readLine().also { s = it } != null) {
                println(s)
            }
        }

        fun fileInOutputDir(file: String) =
            Path.of(outputDir.absolutePath, file).toFile()

        fun writeFileInOutputDir(file: String, content: String) {
            Path.of(outputDir.absolutePath, file).toFile()
                .writeText(content)
        }
    }
}