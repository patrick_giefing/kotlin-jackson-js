import axios, {AxiosResponse} from 'axios';
import {ServerResponse} from 'http';import {JsonParser, JsonStringifier} from 'jackson-js';
import {EntityA} from '';

export class A {
    constructor(private endpoint: () => string = () => "") { }

    public basicQueryParams(stringParam: string, intParam?: number): Promise<AxiosResponse<EntityA, any>> {
        let path = '/basic-query-params/';
        path = this.endpoint() + path;

        const params: { [key: string]: any } = {};
        params['stringParam'] = stringParam;
        params['intParamOptional'] = intParam;
        const headers: { [key: string]: string } = { } = {};

        return axios.request<EntityA>({
            url: path,
            method: 'get',
            params: params,
            headers: headers

            ,transformResponse: (json: string) => {
                const jsonParser = new JsonParser();
                const responseEntity = jsonParser.transform(json, {
                    mainCreator: () => [EntityA]
                });
                return responseEntity;
            }
        })
    }


    headerParam(headerParam: string): Promise<AxiosResponse<void, any>> {
        let path = '/header/';
        path = this.endpoint() + path;

        const params: { [key: string]: any } = {};

        const headers: { [key: string]: string } = { } = {};
        headers['headerParam'] = headerParam;
        return axios.request<void>({
            url: path,
            method: 'get',
            params: params,
            headers: headers


        })
    }


    pathParam(pathParam: string): Promise<AxiosResponse<number, any>> {
        let path = '/{pathParam}/';
        path = path.replace(/pathParam/g, pathParam + "");
        path = this.endpoint() + path;

        const params: { [key: string]: any } = {};

        const headers: { [key: string]: string } = { } = {};

        return axios.request<number>({
            url: path,
            method: 'get',
            params: params,
            headers: headers


        })
    }

}