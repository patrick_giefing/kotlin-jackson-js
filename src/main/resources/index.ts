import {ServiceGenerationTestClassEntityInheritanceRoundtrip} from './service';

const service = new ServiceGenerationTestClassEntityInheritanceRoundtrip(() => "http://localhost:52720");

function b(): A & {thenEntityConverted: (callback: (entity: string) => void) => void} {
    const a = new A() as A & {thenEntityConverted: (callback: (entity: string) => void) => void};
    a.name = "n";
    a.thenEntityConverted = function(callback: (entity: string) => void): void {
        callback("responseEntity-" + Math.random());
    }
    return a
}

const b1 = b()
console.info(b1.thenEntityConverted(responseEntity => console.info(responseEntity)))