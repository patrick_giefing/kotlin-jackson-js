import {JsonTypeInfo, JsonTypeInfoId, JsonTypeInfoAs, JsonSubTypes, JsonFormat, JsonFormatShape, JsonProperty, JsonTypeName, JsonClassType} from 'jackson-js';

	
            @JsonTypeInfo({
              use: JsonTypeInfoId.NAME,
              include: JsonTypeInfoAs.PROPERTY,
              property: "type"
            })
            
	            @JsonSubTypes({
              types: [
                {class: () => Cat, name: 'cat'},
{class: () => Dog, name: 'dog'}
              ]
            })
export class Pet {
@JsonClassType({type: () => [Date]})	
              @JsonFormat({
                shape: JsonFormatShape.STRING,
                pattern: 'YYYY-MM-DD'
              })
	@JsonProperty({value: ''})
	birthDate: Date


@JsonClassType({type: () => [String]})	@JsonProperty({value: ''})
	name: string
}
	@JsonTypeName({value: 'cat'})
export class Cat extends Pet {
@JsonClassType({type: () => [Date]})	@JsonProperty({value: ''})
	birthDate: Date


@JsonClassType({type: () => [Number]})	@JsonProperty({value: ''})
	lifesRemaining: number


@JsonClassType({type: () => [String]})	@JsonProperty({value: ''})
	name: string
}
	@JsonTypeName({value: 'dog'})
export class Dog extends Pet {
@JsonClassType({type: () => [Date]})	@JsonProperty({value: ''})
	birthDate: Date


@JsonClassType({type: () => [String]})	@JsonProperty({value: ''})
	name: string


@JsonClassType({type: () => [Number]})	@JsonProperty({value: 'postmen-bitten'})
	postmenBitten: number
}
