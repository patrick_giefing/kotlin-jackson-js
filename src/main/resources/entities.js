"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.OneStringWithoutAnnotationsTestEntity = void 0;
var jackson_js_1 = require("jackson-js");
var OneStringWithoutAnnotationsTestEntity = (function () {
    function OneStringWithoutAnnotationsTestEntity() {
    }
    __decorate([
        (0, jackson_js_1.JsonProperty)(),
        (0, jackson_js_1.JsonClassType)({ type: function () { return [String]; } })
    ], OneStringWithoutAnnotationsTestEntity.prototype, "stringField");
    return OneStringWithoutAnnotationsTestEntity;
}());
exports.OneStringWithoutAnnotationsTestEntity = OneStringWithoutAnnotationsTestEntity;
//# sourceMappingURL=entities.js.map