var A = (function () {
    function A() {
    }
    return A;
}());
function b() {
    var a = new A();
    a.name = "n";
    a.thenEntityConverted = function (callback) {
        callback("responseEntity-" + Math.random());
    };
    return a;
}
var b1 = b();
console.info(b1.thenEntityConverted(function (responseEntity) { return console.info(responseEntity); }));
//# sourceMappingURL=index.js.map