package es.rick.jacksonjs.entity.annotation

import kotlin.reflect.KClass

@Target(AnnotationTarget.CLASS)
annotation class TsPackage(
    val entityFiles: Array<TsEntityFile> = [],
    val serviceFiles: Array<TsServiceFile> = []
)

annotation class TsEntityFile(
    val fileName: String,
    val entityTypes: Array<KClass<*>>,
    val entityFileImports: Array<TsEntityFile> = []
)

annotation class TsServiceFile(
    val fileName: String,
    val serviceTypes: Array<KClass<*>>,
    val entityFileImports: Array<TsEntityFile> = []
)