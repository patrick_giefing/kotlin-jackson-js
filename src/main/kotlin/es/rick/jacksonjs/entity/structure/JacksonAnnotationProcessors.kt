package es.rick.jacksonjs.entity.structure

import com.fasterxml.jackson.annotation.*
import es.rick.jacksonjs.service.generator.ServiceFileGenerator.Companion.NL
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance

abstract class JacksonAnnotationProcessor<T : Annotation> {
    abstract fun process(annotation: T): JacksonAnnotationProcessorResult
}

class JsonAliasProcessor : JacksonAnnotationProcessor<JsonAlias>() {
    override fun process(jsonAlias: JsonAlias): JacksonAnnotationProcessorResult {
        return JacksonAnnotationProcessorResult(
            annotationDeclaration =
            "@${JsonAlias::class.simpleName}({values: [${jsonAlias.value.joinToString(", ") { "'$it'" }}]})"
        )
    }
}

/*class JsonAppendProcessor : JacksonAnnotationProcessor<JsonAppend>() {}

class JsonAnyGetterProcessor : JacksonAnnotationProcessor<JsonAnyGetter>() {}

class JsonAnySetterProcessor : JacksonAnnotationProcessor<JsonAnySetter>() {}

class JsonBackReferenceProcessor : JacksonAnnotationProcessor<JsonBackReference>() {}

class JsonCreatorProcessor : JacksonAnnotationProcessor<JsonCreator>() {}

class JsonDeserializeProcessor : JacksonAnnotationProcessor<JsonDeserialize>() {}

class JsonFilterProcessor : JacksonAnnotationProcessor<JsonFilter>() {}

class JsonGetterProcessor : JacksonAnnotationProcessor<JsonGetter>() {}

class JsonIdentityInfoProcessor : JacksonAnnotationProcessor<JsonIdentityInfo>() {}

class JsonIdentityReferenceProcessor : JacksonAnnotationProcessor<JsonIdentityReference>() {}

class JsonIgnoreProcessor : JacksonAnnotationProcessor<JsonIgnore>() {}

class JsonIgnorePropertiesProcessor : JacksonAnnotationProcessor<JsonIgnoreProperties>() {}

class JsonIgnoreTypeProcessor : JacksonAnnotationProcessor<JsonIgnoreType>() {}

class JsonIncludeProcessor : JacksonAnnotationProcessor<JsonInclude>() {}

class JsonManagedReferenceProcessor : JacksonAnnotationProcessor<JsonManagedReference>() {}

class JsonNamingProcessor : JacksonAnnotationProcessor<JsonNaming>() {}*/

class JsonPropertyProcessor : JacksonAnnotationProcessor<JsonProperty>() {
    override fun process(jsonProperty: JsonProperty): JacksonAnnotationProcessorResult {
        // TODO
        return JacksonAnnotationProcessorResult(
            annotationDeclaration = "@${JsonProperty::class.simpleName}({value: '${jsonProperty.value}'})"
        )
    }
}

/*class JsonPropertyOrderProcessor : JacksonAnnotationProcessor<JsonPropertyOrder>() {}

class JsonRawValueProcessor : JacksonAnnotationProcessor<JsonRawValue>() {}

class JsonRootNameProcessor : JacksonAnnotationProcessor<JsonRootName>() {}

class JsonSerializeProcessor : JacksonAnnotationProcessor<JsonSerialize>() {}

class JsonSetterProcessor : JacksonAnnotationProcessor<JsonSetter>() {}

class JsonTypeIdResolverProcessor : JacksonAnnotationProcessor<JsonTypeIdResolver>() {}

class JsonUnwrappedProcessor : JacksonAnnotationProcessor<JsonUnwrapped>() {}

class JsonValueProcessor : JacksonAnnotationProcessor<JsonValue>() {}

class JsonInjectProcessor:JacksonAnnotationProcessor<JsonInject>() {}

class JsonViewProcessor : JacksonAnnotationProcessor<JsonView>() {}*/


// TODO https://pichillilorenzo.github.io/jackson-js/latest/modules/decorators.html#jsonformat
class JsonFormatProcessor : JacksonAnnotationProcessor<JsonFormat>() {
    override fun process(annotation: JsonFormat): JacksonAnnotationProcessorResult {
        return JacksonAnnotationProcessorResult(
            annotationDeclaration = """
              @JsonFormat({
                shape: JsonFormatShape.${annotation.shape.name},
                pattern: '${
                annotation.pattern
                        // dayjs uses a different format than jackson
                    .replace("yyyy", "YYYY")
                    .replace("dd", "DD")
            }'
              })""",
            usedJacksonJsTypes = setOf("JsonFormatShape")
        )
    }

}

class JsonTypeIdProcessor : JacksonAnnotationProcessor<JsonTypeId>() {
    override fun process(annotation: JsonTypeId): JacksonAnnotationProcessorResult {
        return JacksonAnnotationProcessorResult(
            annotationDeclaration = "@JsonTypeId()"
        )
    }
}

class JsonSubTypesProcessor : JacksonAnnotationProcessor<JsonSubTypes>() {
    override fun process(annotation: JsonSubTypes): JacksonAnnotationProcessorResult {
        return JacksonAnnotationProcessorResult(
            annotationDeclaration = """
            @JsonSubTypes({
              types: [
                ${
                annotation.value.joinToString(",$NL") {
                    """{class: () => ${it.value.simpleName}${if (it.name.isNotEmpty()) ", name: '${it.name}'" else ""}}"""
                }
            }
              ]
            })
            """.trimIndent()
        )
    }
}

class JsonTypeInfoProcessor : JacksonAnnotationProcessor<JsonTypeInfo>() {
    override fun process(annotation: JsonTypeInfo): JacksonAnnotationProcessorResult {
        // TODO add imports for JsonTypeInfoId, JsonTypeInfoAs
        return JacksonAnnotationProcessorResult(
            annotationDeclaration = """
            @JsonTypeInfo({
              use: JsonTypeInfoId.${annotation.use.name},
              include: JsonTypeInfoAs.${annotation.include.name},
              property: "${annotation.property}"
            })
            """,
            usedJacksonJsTypes = setOf("JsonTypeInfoId", "JsonTypeInfoAs")
        )
    }
}

class JsonTypeNameProcessor : JacksonAnnotationProcessor<JsonTypeName>() {
    override fun process(annotation: JsonTypeName): JacksonAnnotationProcessorResult {
        return JacksonAnnotationProcessorResult(
            annotationDeclaration = "@JsonTypeName({value: '${annotation.value}'})"
        )
    }
}

val annotationProcessorMappings: Map<KClass<out Annotation>, KClass<out JacksonAnnotationProcessor<*>>> =
    mapOf(
        JsonAlias::class to JsonAliasProcessor::class,
        JsonTypeId::class to JsonTypeIdProcessor::class,
        JsonSubTypes::class to JsonSubTypesProcessor::class,
        JsonTypeInfo::class to JsonTypeInfoProcessor::class,
        JsonTypeName::class to JsonTypeNameProcessor::class,
        JsonProperty::class to JsonPropertyProcessor::class,
        JsonFormat::class to JsonFormatProcessor::class
    )

fun processAnnotation(annotation: Annotation): JacksonAnnotationProcessorResult? {
    val processorClass = annotationProcessorMappings[annotation.annotationClass] ?: return null
    val processorInstance = processorClass.createInstance()
    val processMethod = processorClass.javaObjectType.declaredMethods.filter { it.name == "process" }.first()
    val result = processMethod.invoke(processorInstance, annotation) as JacksonAnnotationProcessorResult?
    return result
}

annotation class JsonClassType()

data class JacksonAnnotationProcessorResult(
    val annotationDeclaration: String,
    val usedJacksonJsTypes: Set<String?> = setOf()
)