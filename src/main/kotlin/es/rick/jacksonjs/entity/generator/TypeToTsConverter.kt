package es.rick.jacksonjs.entity.generator

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*
import kotlin.reflect.KType
import kotlin.reflect.full.isSubtypeOf
import kotlin.reflect.full.starProjectedType
import kotlin.reflect.full.withNullability
import kotlin.reflect.jvm.jvmErasure

abstract class TypeToTsConverter {
    fun convert(type: KType): String {
        val isIterable = Iterable::class.java.isAssignableFrom(type.jvmErasure.java) ||
                java.lang.Iterable::class.java.isAssignableFrom(type.jvmErasure.java)
        val isKotlinArray = Array::class.java.isAssignableFrom(type.jvmErasure.java) ||
                type.isSubtypeOf(Array::class.starProjectedType)
        val isJavaArray = type.jvmErasure.java.isArray
        if (isIterable || isKotlinArray) {
            val iterableElementType = convert(type.arguments[0].type!!)
            return array(iterableElementType)
        } else if (isJavaArray) {
            // TODO
        } else if (type.arguments.isNotEmpty()) {
            error("Generics not yet implemented")
        }
        // TODO Java primitives
        val isDate = isDate(type)
        val isBoolean = isBoolean(type)
        val isNumber = isNumber(type)
        val isString = isString(type)
        if (isDate) {
            return type("Date")
        }
        if (isBoolean) {
            return type(boolean())
        }
        if (isNumber) {
            return type(number())
        }
        if (isString) {
            return type(string())
        }
        if (isUnit(type)) {
            return type("void")
        }
        return type(type.jvmErasure.simpleName!!)
    }

    protected abstract fun array(componentTypeName: String): String

    protected abstract fun type(typeName: String): String

    protected abstract fun boolean(): String

    protected abstract fun string(): String

    protected abstract fun number(): String
}

class TypeToTsDeclarationConverter : TypeToTsConverter() {
    override fun array(componentTypeName: String): String {
        return "$componentTypeName[]"
    }

    override fun type(typeName: String): String {
        return typeName
    }

    override fun boolean() = "boolean"

    override fun string() = "string"

    override fun number() = "number"
}

class TypeToJacksonJsJsonClassTypeConverter : TypeToTsConverter() {
    override fun array(componentTypeName: String): String {
        return "[Array, $componentTypeName]"
    }

    override fun type(typeName: String): String {
        return "[$typeName]"
    }

    override fun boolean() = "Boolean"

    override fun string() = "String"

    override fun number() = "Number"
}

fun isDate(type: KType): Boolean {
    val typeNotNull = type.withNullability(false)
    return typeNotNull.isSubtypeOf(Date::class.starProjectedType) ||
            typeNotNull.isSubtypeOf(LocalDate::class.starProjectedType) ||
            typeNotNull.isSubtypeOf(LocalTime::class.starProjectedType) ||
            typeNotNull.isSubtypeOf(LocalDateTime::class.starProjectedType)
}

fun isBoolean(type: KType) = type.withNullability(false).isSubtypeOf(Boolean::class.starProjectedType)

fun isString(type: KType) = type.withNullability(false).isSubtypeOf(String::class.starProjectedType)

fun isNumber(type: KType) = type.withNullability(false).isSubtypeOf(Number::class.starProjectedType)

fun isUnit(type: KType) = type.withNullability(false) == Unit::class.starProjectedType

fun isAny(type: KType) = type.withNullability(false) == Any::class.starProjectedType
