package es.rick.jacksonjs.entity.generator

import kotlin.reflect.KType
import kotlin.reflect.jvm.jvmErasure

class TestFileGenerator {
    fun generate(
        inputFile: String, outputFile: String,
        testEntity: KType, testEntityFileName: String
    ): String {
        val testEntityTypeName = testEntity.jvmErasure.simpleName ?: "UnknownEntityType"
        val typeDeclarationString = TypeToJacksonJsJsonClassTypeConverter().convert(testEntity)
        return """
import {JsonParser, JsonStringifier} from 'jackson-js';
import {readFileSync, writeFileSync} from 'fs';
import {$testEntityTypeName} from './${testEntityFileName.removeSuffix(".ts")}';

const json = readFileSync("${inputFile.replace('\\', '/')}", 'utf8')

const jsonParser = new JsonParser();
const ${testEntityTypeName.decapitalize()} = jsonParser.parse(json, {
    mainCreator: () => $typeDeclarationString
});

const jsonStringifier = new JsonStringifier();
const jsonResult = jsonStringifier.stringify(${testEntityTypeName.decapitalize()}, {
    mainCreator: () => $typeDeclarationString
});

writeFileSync("${outputFile.replace('\\', '/')}", jsonResult, 'utf8')
""".trimIndent()
    }
}