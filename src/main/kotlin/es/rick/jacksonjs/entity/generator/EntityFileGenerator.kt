package es.rick.jacksonjs.entity.generator

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import es.rick.jacksonjs.FileGenerator
import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.entity.structure.JsonClassType
import es.rick.jacksonjs.entity.structure.processAnnotation
import java.lang.reflect.Field
import kotlin.reflect.KAnnotatedElement
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.declaredMembers
import kotlin.reflect.full.findAnnotations
import kotlin.reflect.jvm.javaField
import kotlin.reflect.jvm.jvmErasure
import kotlin.reflect.jvm.kotlinProperty

class EntityFileGenerator : FileGenerator() {
    fun processEntityFiles(entityFiles: Array<TsEntityFile>): Array<TsFileResult> {
        return entityFiles.map { entityFile ->
            val usedJacksonJsTypes = mutableSetOf<String?>()
            val content = StringBuffer()
            entityFile.entityTypes.forEach { entityType ->
                entityType.annotations.forEach { annotation ->
                    val annotationDeclaration = processAnnotation(annotation)
                    annotationDeclaration?.let {
                        content.append("\t${annotationDeclaration.annotationDeclaration}$NL")
                        usedJacksonJsTypes.add(annotation.annotationClass.simpleName)
                        usedJacksonJsTypes.addAll(it.usedJacksonJsTypes)
                    }
                }
                var extends = ""
                if(entityType.supertypes.isNotEmpty()) {
                    val supertype = entityType.supertypes.first()
                    if(!isAny(supertype)) {
                        val supertypeTs = TypeToTsDeclarationConverter().convert(supertype)
                        extends = " extends $supertypeTs"
                    }
                }
                content.append("export class ${entityType.simpleName}$extends {$NL")
                entityType.declaredMemberProperties.forEachIndexed { index, property ->
                    val field = property.javaField!!
                    val tsType = TypeToTsDeclarationConverter().convert(field.kotlinProperty!!.returnType)
                    val jacksonType = TypeToJacksonJsJsonClassTypeConverter().convert(field.kotlinProperty!!.returnType)
                    val fieldAnnotations = (property.annotations + property.getter.annotations + field.annotations).toMutableSet()
                    val jsonPropertyDefined = field.annotations.any { it.annotationClass == JsonProperty::class }
                    if (!jsonPropertyDefined) {
                        fieldAnnotations += JsonProperty::class.createInstance()
                    }
                    val jsonClassTypeDefined = field.annotations.any { it.annotationClass == JsonClassType::class }
                    if (!jsonClassTypeDefined) {
                        fieldAnnotations += JsonClassType()
                    }
                    run {
                        content.append("@JsonClassType({type: () => ${jacksonType}})")
                    }
                    fieldAnnotations.forEach { annotation ->
                        val annotationDeclaration = processAnnotation(annotation)
                        annotationDeclaration?.let {
                            content.append("\t${annotationDeclaration.annotationDeclaration}$NL")
                            usedJacksonJsTypes.add(annotation.annotationClass.simpleName)
                            usedJacksonJsTypes.addAll(it.usedJacksonJsTypes)
                        }
                    }
                    val optionalMarker = if (field.kotlinProperty!!.returnType.isMarkedNullable) "?" else ""
                    content.append("\t${field.name}${optionalMarker}: $tsType")
                    val isLastField = index == entityType.javaObjectType.declaredFields.size - 1
                    if (!isLastField) {
                        content.append("$NL$NL")
                    }
                    content.append(NL)
                }
                content.append("}$NL")
            }
            val jacksonJsImportClasses = usedJacksonJsTypes + JsonClassType::class.java.simpleName

            val contentFile = StringBuffer()
            contentFile.append("import {${jacksonJsImportClasses.joinToString(", ")}} from 'jackson-js';$NL")
            contentFile.append(createEntityImports(entityFile))
            contentFile.append(NL)
            contentFile.append(content)
            TsFileResult(
                fileName = entityFile.fileName,
                content = contentFile.toString()
            )
        }.toTypedArray()
    }

    private fun createEntityImports(entityFile: TsEntityFile): String {
        return ""
    }

    companion object {
        const val NL = "\r\n"

        fun getUsedClasses(clazz: KClass<*>): Set<KClass<*>> {
            val usedClasses = mutableSetOf<KClass<*>>()
            appendUsedClassesInAnnotations(usedClasses, clazz)
            val supertypes = clazz.supertypes
            val declaredMembers = clazz.declaredMembers
            usedClasses.add(clazz)
            supertypes.forEach {
                usedClasses.add(it.jvmErasure)
                appendTypeArgumentsRecursively(usedClasses, it)
            }
            declaredMembers.forEach {
                usedClasses.add(it.returnType.jvmErasure)
                appendTypeArgumentsRecursively(usedClasses, it.returnType)
                appendUsedClassesInAnnotations(usedClasses, it)
            }
            return usedClasses
        }

        @OptIn(ExperimentalStdlibApi::class)
        fun appendUsedClassesInAnnotations(usedClasses: MutableSet<KClass<*>>, annotatedElement: KAnnotatedElement) {
            val annJsonSubTypes = annotatedElement.findAnnotations(JsonSubTypes::class)
            annJsonSubTypes.forEach {
                usedClasses.addAll(it.value.map { it.value })
            }
        }
    }
}

data class TsEntityResult(
    val entityType: KClass<*>,
    val tsEntityAnnotationDeclarations: Set<String>,
    val fieldResults: List<TsFieldResult>
)

data class TsFieldResult(
    val field: Field,
    val tsFieldAnnotationDeclarations: Set<String>
)

data class TsFileResult(
    val fileName: String,
    val content: String
)
