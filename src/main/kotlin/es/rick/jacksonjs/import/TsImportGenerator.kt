package es.rick.jacksonjs.import

import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.service.generator.ServiceFileGenerator.Companion.NL
import java.time.LocalDate
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.jvm.jvmErasure

class TsImportGenerator {
    companion object {
        fun generateImportStatements(
            definedClasses: Set<KClass<*>>,
            usedClasses: Set<KClass<*>>,
            entityFileImports: Collection<TsEntityFile>
        ): String {
            val importsToGenerate = findImports(definedClasses, usedClasses, entityFileImports)
            val generatedImportString = generateImportString(importsToGenerate)
            return generatedImportString
        }

        fun findImports(
            definedClasses: Set<KClass<*>>,
            usedClasses: Set<KClass<*>>,
            entityFileImports: Collection<TsEntityFile>
        ): Map<String, List<KClass<*>>> {
            val usedNonPrimitiveClasses = filterNotPrimitiveClasses(usedClasses)
            val usedClassesNeededImport = usedNonPrimitiveClasses.filter { !definedClasses.contains(it) }
            val allImportableClasses = entityFileImports.flatMap { it.entityTypes.toList() }
            val usedClassesNotFoundInImports = usedClassesNeededImport.filter { !allImportableClasses.contains(it) }
            if (usedClassesNotFoundInImports.isNotEmpty()) {
                error(
                    "Some classes where not found in any provided import: ${
                        usedClassesNotFoundInImports.map { it.qualifiedName }.joinToString(", ")
                    }"
                )
            }
            val importsToGenerate = entityFileImports.mapNotNull {
                val providedImportClassesByEntityFile =
                    it.entityTypes.toList().filter { usedClassesNeededImport.contains(it) }
                if (providedImportClassesByEntityFile.isNotEmpty())
                    it.fileName to providedImportClassesByEntityFile
                else
                    null
            }.toMap()
            return importsToGenerate
        }

        fun generateImportString(classImports: Map<String, Collection<KClass<*>>>): String {
            val importString = classImports.map { (fileName, classesToImport) ->
                "import {${classesToImport.map { it.simpleName }.joinToString(", ")}} from '${fileName.removeSuffix(".ts")}';"
            }.joinToString(NL)
            return importString
        }

        private val primitiveClassPackages =
            setOf(LocalDate::class, Date::class, Array::class, Unit::class).map { it.java.packageName }

        // TODO check for Array/Collection/Map-Component(s)
        fun isPrimitiveClass(clazz: KClass<*>): Boolean {
            return primitiveClassPackages.any {
                val qName = clazz.qualifiedName!!
                qName.startsWith(it)
            }
        }

        fun filterNotPrimitiveClasses(classes: Collection<KClass<*>>): Set<KClass<*>> {
            return classes.filter { clazz ->
                val qName = clazz.qualifiedName!!
                primitiveClassPackages.none { qName.startsWith(it) }
            }.toSet()
        }

        fun filterNotPrimitiveTypes(types: Collection<KType>): Set<KType> {
            return types.filter { type ->
                val qName = type.jvmErasure.qualifiedName ?: ""
                primitiveClassPackages.none { qName.startsWith(it) }
            }.toSet()
        }
    }
}