package es.rick.jacksonjs

import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.jvm.jvmErasure

abstract class FileGenerator {
    companion object {
        @JvmStatic
        protected fun appendTypeArgumentsRecursively(usedClasses: MutableSet<KClass<*>>, type: KType) {
            val typeArguments = type.arguments
            typeArguments.forEach {
                it.type?.let {
                    appendTypeArgumentsRecursively(usedClasses, it)
                    usedClasses.add(it.jvmErasure)
                }
            }
        }
    }
}