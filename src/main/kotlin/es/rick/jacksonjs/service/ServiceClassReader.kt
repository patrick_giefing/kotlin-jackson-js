@file:OptIn(ExperimentalStdlibApi::class)

package es.rick.jacksonjs.service

import es.rick.jacksonjs.service.model.RestServiceClass
import es.rick.jacksonjs.service.model.RestServiceMethod
import es.rick.jacksonjs.service.model.RestServiceMethodParameter
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter

abstract class ServiceClassReader() {
    abstract fun readServiceClass(clazz: KClass<*>): RestServiceClass
}

abstract class ServiceMethodReader() {
    abstract fun readServiceMethod(method: KFunction<*>): RestServiceMethod

    protected fun getSingleParameterOrFail(
        method: KFunction<*>,
        params: List<RestServiceMethodParameter>
    ): RestServiceMethodParameter {
        if (params.isEmpty()) {
            error("Method ${method.name} doesn't have any parameter annotations")
        }
        if (params.size > 1) {
            error(
                "Method ${method.name} has multiple parameter annotations: ${
                    params.joinToString(", ") { it.toString() }
                }"
            )
        }
        return params.first()
    }
}

abstract class ServiceMethodParameterReader() {
    abstract fun readServiceMethodParameter(param: KParameter): List<RestServiceMethodParameter>
}

