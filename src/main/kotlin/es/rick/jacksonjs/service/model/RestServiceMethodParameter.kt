package es.rick.jacksonjs.service.model

import kotlin.reflect.KType

abstract class RestServiceMethodParameter(
    val fieldName: String? = null,
    val type: KType,
    val defaultValue: String? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RestServiceMethodParameter

        if (fieldName != other.fieldName) return false
        if (type != other.type) return false
        if (defaultValue != other.defaultValue) return false

        return true
    }

    override fun hashCode(): Int {
        var result = fieldName?.hashCode() ?: 0
        result = 31 * result + type.hashCode()
        result = 31 * result + (defaultValue?.hashCode() ?: 0)
        return result
    }
}