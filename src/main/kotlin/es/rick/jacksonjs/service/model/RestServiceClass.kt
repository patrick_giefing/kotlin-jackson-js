package es.rick.jacksonjs.service.model

import kotlin.reflect.KType

data class RestServiceClass(
    val serviceName: String = "RestService",
    val path: String? = null,
    val methods: List<RestServiceMethod> = emptyList()
) {
    fun getUsedTypes(): Set<KType> {
        return methods.map { getUsedTypes() }.flatten().toSet()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RestServiceClass

        if (serviceName != other.serviceName) return false
        if (path != other.path) return false
        if (methods != other.methods) return false

        return true
    }

    override fun hashCode(): Int {
        var result = serviceName.hashCode()
        result = 31 * result + (path?.hashCode() ?: 0)
        result = 31 * result + methods.hashCode()
        return result
    }

}