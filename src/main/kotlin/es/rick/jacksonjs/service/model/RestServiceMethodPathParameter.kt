package es.rick.jacksonjs.service.model

import kotlin.reflect.KType

class RestServiceMethodPathParameter(
    val pathParam: String? = null,
    type: KType,
    fieldName: String? = null,
    defaultValue: String? = null
) : RestServiceMethodParameter(fieldName, type, defaultValue) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as RestServiceMethodPathParameter

        if (pathParam != other.pathParam) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (pathParam?.hashCode() ?: 0)
        return result
    }
}