package es.rick.jacksonjs.service.model

import kotlin.reflect.KType

class RestServiceMethodCookieParameter(
    val cookieName: String? = null,
    type: KType,
    fieldName: String? = null,
    defaultValue: String? = null
) : RestServiceMethodParameter(fieldName, type, defaultValue) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RestServiceMethodCookieParameter

        if (cookieName != other.cookieName) return false

        return true
    }

    override fun hashCode(): Int {
        return cookieName?.hashCode() ?: 0
    }
}