package es.rick.jacksonjs.service.model

import kotlin.reflect.KType

class RestServiceMethodBodyParameter(
    type: KType,
    fieldName: String? = null
) : RestServiceMethodParameter(fieldName, type, null) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false
        return true
    }
}