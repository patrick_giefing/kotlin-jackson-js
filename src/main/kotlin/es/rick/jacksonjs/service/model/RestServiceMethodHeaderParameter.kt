package es.rick.jacksonjs.service.model

import kotlin.reflect.KType

class RestServiceMethodHeaderParameter(
    val headerName: String? = null,
    type: KType,
    fieldName: String? = null,
    defaultValue: String? = null
) : RestServiceMethodParameter(fieldName, type, defaultValue) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RestServiceMethodHeaderParameter

        if (headerName != other.headerName) return false

        return true
    }

    override fun hashCode(): Int {
        return headerName?.hashCode() ?: 0
    }
}