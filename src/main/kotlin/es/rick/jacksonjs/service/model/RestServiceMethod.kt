package es.rick.jacksonjs.service.model

import kotlin.reflect.KType

data class RestServiceMethod(
    val methodName: String? = null,
    val path: String? = null,
    val method: String? = null,
    val responseType: KType,
    val parameters: List<RestServiceMethodParameter> = emptyList()
) {
    fun getUsedTypes(): Set<KType> {
        return (parameters.map { it.type } + responseType).toSet()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RestServiceMethod

        if (methodName != other.methodName) return false
        if (path != other.path) return false
        if (method != other.method) return false
        if (responseType != other.responseType) return false
        if (parameters != other.parameters) return false

        return true
    }

    override fun hashCode(): Int {
        var result = methodName?.hashCode() ?: 0
        result = 31 * result + (path?.hashCode() ?: 0)
        result = 31 * result + (method?.hashCode() ?: 0)
        result = 31 * result + responseType.hashCode()
        result = 31 * result + parameters.hashCode()
        return result
    }
}