package es.rick.jacksonjs.service

import es.rick.jacksonjs.service.model.*
import javax.ws.rs.*
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.full.findAnnotations
import kotlin.reflect.full.valueParameters

@OptIn(ExperimentalStdlibApi::class)
class JaxRsServiceClassReader : ServiceClassReader() {
    override fun readServiceClass(type: KClass<*>): RestServiceClass {
        val annPath = type.findAnnotations(Path::class).firstOrNull()

        val methodReader = JaxRsServiceMethodReader()
        val methods = type.declaredFunctions.map { methodReader.readServiceMethod(it) }
        return RestServiceClass(serviceName = type.simpleName!!, path = annPath?.value, methods = methods)
    }
}

@OptIn(ExperimentalStdlibApi::class)
class JaxRsServiceMethodReader : ServiceMethodReader() {
    override fun readServiceMethod(method: KFunction<*>): RestServiceMethod {
        val annPath = method.findAnnotations(Path::class).firstOrNull()
        val httpMethod = getHttpMethod(method)
        val paramReader = JaxRsServiceMethodParameterReader()
        val params =
            method.valueParameters.map { getSingleParameterOrFail(method, paramReader.readServiceMethodParameter(it)) }
        return RestServiceMethod(
            methodName = method.name,
            path = annPath?.value,
            method = httpMethod,
            responseType = method.returnType,
            parameters = params
        )
    }

    fun getHttpMethod(method: KFunction<*>): String? {
        val httpMethod = if (method.findAnnotations(GET::class).isNotEmpty()) {
            HttpMethod.GET
        } else if (method.findAnnotations(POST::class).isNotEmpty()) {
            HttpMethod.POST
        } else if (method.findAnnotations(PUT::class).isNotEmpty()) {
            HttpMethod.PUT
        } else if (method.findAnnotations(DELETE::class).isNotEmpty()) {
            HttpMethod.DELETE
        } else if (method.findAnnotations(PATCH::class).isNotEmpty()) {
            HttpMethod.PATCH
        } else if (method.findAnnotations(HEAD::class).isNotEmpty()) {
            HttpMethod.HEAD
        } else if (method.findAnnotations(OPTIONS::class).isNotEmpty()) {
            HttpMethod.OPTIONS
        } else {
            null
        }
        return httpMethod
    }
}

@OptIn(ExperimentalStdlibApi::class)
class JaxRsServiceMethodParameterReader : ServiceMethodParameterReader() {
    override fun readServiceMethodParameter(param: KParameter): List<RestServiceMethodParameter> {
        val fieldName = param.name
        val defaultValue = param.findAnnotations(DefaultValue::class).firstOrNull()?.value
        val params = param.findAnnotations(QueryParam::class)
            .map { RestServiceMethodQueryParameter(it.value, param.type, fieldName, defaultValue) } +
                param.findAnnotations(PathParam::class)
                    .map { RestServiceMethodPathParameter(it.value, param.type, fieldName, defaultValue) } +
                param.findAnnotations(HeaderParam::class)
                    .map { RestServiceMethodHeaderParameter(it.value, param.type, fieldName, defaultValue) } +
                param.findAnnotations(CookieParam::class)
                    .map { RestServiceMethodCookieParameter(it.value, param.type, fieldName, defaultValue) }
        return params
    }
}