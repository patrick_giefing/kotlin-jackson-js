package es.rick.jacksonjs.service

import es.rick.jacksonjs.service.model.*
import org.springframework.web.bind.annotation.*
import javax.ws.rs.HttpMethod
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.full.findAnnotations
import kotlin.reflect.full.valueParameters

@OptIn(ExperimentalStdlibApi::class)
class SpringWebServiceClassReader : ServiceClassReader() {
    override fun readServiceClass(clazz: KClass<*>): RestServiceClass {
        val annPath = clazz.findAnnotations(RequestMapping::class).firstOrNull()
        val methodReader = SpringWebServiceMethodReader(annPath?.method)
        val methods = clazz.declaredFunctions.map { methodReader.readServiceMethod(it) }
        return RestServiceClass(
            serviceName = clazz.simpleName!!,
            path = getPath(annPath?.path, annPath?.value),
            methods = methods
        )
    }
}

@OptIn(ExperimentalStdlibApi::class)
class SpringWebServiceMethodReader(private val typeRequestMethods: Array<RequestMethod>?) : ServiceMethodReader() {
    override fun readServiceMethod(method: KFunction<*>): RestServiceMethod {
        var path: String? = null
        var httpMethod: String? = null
        val annGet = method.findAnnotations(GetMapping::class).firstOrNull()
        val annPost = method.findAnnotations(PostMapping::class).firstOrNull()
        val annPut = method.findAnnotations(PutMapping::class).firstOrNull()
        val annDelete = method.findAnnotations(DeleteMapping::class).firstOrNull()
        val annPatch = method.findAnnotations(PatchMapping::class).firstOrNull()
        val annRequest = method.findAnnotations(RequestMapping::class).firstOrNull()
        if (annGet != null) {
            path = getPath(annGet.path, annGet.value)
            httpMethod = HttpMethod.GET
        } else if (annPost != null) {
            path = getPath(annPost.path, annPost.value)
            httpMethod = HttpMethod.POST
        } else if (annPut != null) {
            path = getPath(annPut.path, annPut.value)
            httpMethod = HttpMethod.PUT
        } else if (annDelete != null) {
            path = getPath(annDelete.path, annDelete.value)
            httpMethod = HttpMethod.DELETE
        } else if (annPatch != null) {
            path = getPath(annPatch.path, annPatch.value)
            httpMethod = HttpMethod.PATCH
        } else if (annRequest != null) {
            path = getPath(annRequest.path, annRequest.value)
            httpMethod = annRequest.method.firstOrNull()?.name ?: typeRequestMethods?.firstOrNull()?.name
                    ?: error("No HTTP method set for method ${method.name}")
        }
        val paramReader = SpringWebServiceMethodParameterReader()
        val params = method.valueParameters.map {
            getSingleParameterOrFail(method, paramReader.readServiceMethodParameter(it))
        }
        return RestServiceMethod(
            methodName = method.name,
            path = path,
            method = httpMethod,
            responseType = method.returnType,
            parameters = params
        )
    }
}

@OptIn(ExperimentalStdlibApi::class)
class SpringWebServiceMethodParameterReader : ServiceMethodParameterReader() {
    override fun readServiceMethodParameter(param: KParameter): List<RestServiceMethodParameter> {
        val fieldName = param.name
        val params =
            param.findAnnotations(RequestParam::class).map {
                RestServiceMethodQueryParameter(
                    firstNotEmpty(it.name, it.value),
                    param.type,
                    fieldName,
                    checkDefault(it.defaultValue)
                )
            } +
                    param.findAnnotations(PathVariable::class).map {
                        RestServiceMethodPathParameter(firstNotEmpty(it.name, it.value), param.type, fieldName, null)
                    } +
                    param.findAnnotations(RequestHeader::class).map {
                        RestServiceMethodHeaderParameter(
                            firstNotEmpty(it.name, it.value),
                            param.type,
                            fieldName,
                            checkDefault(it.defaultValue)
                        )
                    } +
                    param.findAnnotations(CookieValue::class).map {
                        RestServiceMethodCookieParameter(
                            firstNotEmpty(it.name, it.value),
                            param.type,
                            fieldName,
                            checkDefault(it.defaultValue)
                        )
                    } +
                    param.findAnnotations(RequestBody::class).map {
                        RestServiceMethodBodyParameter(
                            param.type,
                            fieldName
                        )
                    }
        return params
    }
}

fun checkDefault(default: String): String? {
    if (default == ValueConstants.DEFAULT_NONE)
        return null
    return default
}

fun firstNotEmpty(vararg ss: String): String? {
    for (s in ss)
        if (s.isNotEmpty())
            return s
    return null
}

fun getPath(paths1: Array<String>?, paths2: Array<String>?): String? {
    val paths = (paths1 ?: emptyArray()) + (paths2 ?: emptyArray())
    paths.sortBy { it.length }
    val path = paths.lastOrNull() // use any path
    return path
}