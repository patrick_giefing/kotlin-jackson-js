package es.rick.jacksonjs.service.generator

import es.rick.jacksonjs.FileGenerator
import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.service.model.RestServiceClass
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.jvm.jvmErasure

abstract class ServiceFileGenerator : FileGenerator() {
    abstract fun generateServiceStub(serviceClass: RestServiceClass, tsEntityImportFiles: Collection<TsEntityFile>): String

    companion object {
        const val NL = "\r\n"

        fun getUsedClasses(serviceClass: RestServiceClass): Set<KClass<*>> {
            // TODO service super classes not implemented
            val usedClasses = mutableSetOf<KClass<*>>()
            serviceClass.methods.forEach {
                usedClasses.add(it.responseType.jvmErasure)
                appendTypeArgumentsRecursively(usedClasses, it.responseType)
                it.parameters.forEach {
                    usedClasses.add(it.type.jvmErasure)
                    appendTypeArgumentsRecursively(usedClasses, it.type)
                }
            }
            return usedClasses
        }

        fun getUsedClasses(type: KType): Set<KClass<*>> {
            // TODO service super classes not implemented
            val usedClasses = mutableSetOf<KClass<*>>()
            val declaredFunctions = type.jvmErasure.declaredFunctions
            declaredFunctions.forEach {
                usedClasses.add(it.returnType.jvmErasure)
                appendTypeArgumentsRecursively(usedClasses, it.returnType)
                it.parameters.forEach {
                    usedClasses.add(it.type.jvmErasure)
                    appendTypeArgumentsRecursively(usedClasses, it.type)
                }
            }
            return usedClasses
        }

        fun combinePaths(vararg paths: String?): String {
            val joinedPath = paths.joinToString("/") { it ?: "" }.replace("//", "/")
            return joinedPath
        }
    }
}