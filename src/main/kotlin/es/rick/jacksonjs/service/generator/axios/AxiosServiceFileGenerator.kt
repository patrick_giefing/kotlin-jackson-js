package es.rick.jacksonjs.service.generator.axios

import es.rick.jacksonjs.entity.annotation.TsEntityFile
import es.rick.jacksonjs.entity.generator.TypeToJacksonJsJsonClassTypeConverter
import es.rick.jacksonjs.entity.generator.TypeToTsDeclarationConverter
import es.rick.jacksonjs.entity.generator.isUnit
import es.rick.jacksonjs.import.TsImportGenerator
import es.rick.jacksonjs.service.generator.ServiceFileGenerator
import es.rick.jacksonjs.service.model.*

// TODO Request injector
class AxiosServiceFileGenerator : ServiceFileGenerator() {
    private val typeToTsDeclarationConverter = TypeToTsDeclarationConverter()
    private val typeToJacksonJsJsonClassTypeConverter = TypeToJacksonJsJsonClassTypeConverter()

    override fun generateServiceStub(serviceClass: RestServiceClass, tsEntityImportFiles: Collection<TsEntityFile>): String {
        val sb = StringBuilder()
        val usedClasses = getUsedClasses(serviceClass)
        val importString = TsImportGenerator.generateImportStatements(
            setOf(), // TODO other referenced classes in same TS file?
            usedClasses,
            tsEntityImportFiles
        )
        sb.append("import axios, {AxiosResponse} from 'axios';$NL") // TODO only import if used
        sb.append("import {ServerResponse} from 'http';$NL") // TODO only import if used
        sb.append("import {JsonParser, JsonStringifier} from 'jackson-js';$NL") // TODO only import if used
        sb.append("const dayjs = require('dayjs');$NL") // TODO only import if (Local)Date(Time) is used
        if (importString.isNotEmpty()) {
            sb.append(importString)
        }
        sb.append("$NL$NL")
        sb.append("export class ${serviceClass.serviceName} {$NL")
        sb.append("\tconstructor(private endpoint: () => string = () => \"\") { }$NL$NL")
        serviceClass.methods.forEachIndexed { index, restServiceMethod ->
            if (index > 0) {
                sb.append("$NL$NL")
            }
            val methodBody = generateServiceMethodStub(serviceClass.path, restServiceMethod)
            sb.append(methodBody)
        }
        sb.append("$NL}$NL")
        return sb.toString()
    }

    /**
     * Not all parameters make sense in the function declaration
     */
    fun filterParamsForDeclaration(params: List<RestServiceMethodParameter>) =
        params.filter { it !is RestServiceMethodCookieParameter }

    /**
     * @see https://github.com/axios/axios
     */
    private fun generateServiceMethodStub(parentPath: String?, serviceMethod: RestServiceMethod): String {
        if (serviceMethod.method.isNullOrEmpty()) return ""
        val method = serviceMethod.method.toLowerCase()
        val definedParams = filterParamsForDeclaration(serviceMethod.parameters)

        val parameterString = definedParams.joinToString(", ") {
            val jsonClassType = typeToTsDeclarationConverter.convert(it.type)
            "${it.fieldName}${if (it.type.isMarkedNullable) "?" else ""}: $jsonClassType"
        }
        val bodyParam =
            serviceMethod.parameters.filterIsInstance(RestServiceMethodBodyParameter::class.java).firstOrNull()
        var requestEntityBody = ""
        if (bodyParam != null) {
            val isRequestBodyTypePrimitive = isUnit(bodyParam.type) // TODO isPrimitiveClass(bodyParam.type.jvmErasure)
            if (!isRequestBodyTypePrimitive) {
                val requestBodyTypeJacksonTypeDeclaration = typeToJacksonJsJsonClassTypeConverter.convert(bodyParam.type)
                requestEntityBody = """
                const jsonStringifier = new JsonStringifier();
                const jsonData = ${bodyParam.fieldName} ? jsonStringifier.transform(${bodyParam.fieldName}, {
                    mainCreator: () => $requestBodyTypeJacksonTypeDeclaration,
                    dateLibrary: dayjs
                }) : null;
            """.trimIndent()
            }
        }

        val isResponseTypePrimitive = isUnit(serviceMethod.responseType) // TODO isPrimitiveClass(serviceMethod.responseType.jvmErasure)
        var successTransformerResponseBody = ""
        val responseBodyTsDeclaration = typeToTsDeclarationConverter.convert(serviceMethod.responseType)
        if (!isResponseTypePrimitive) {
            val responseTypeJacksonTypeDeclaration = typeToJacksonJsJsonClassTypeConverter.convert(serviceMethod.responseType)
            successTransformerResponseBody = """
                (json: string) => {
                    const jsonParser = new JsonParser();
                    const responseEntity = jsonParser.parse(json, {
                        mainCreator: () => $responseTypeJacksonTypeDeclaration
                    });
                    return responseEntity;
                }
            """.trimIndent()
        }
        val queryParams = serviceMethod.parameters.filterIsInstance(RestServiceMethodQueryParameter::class.java)
        val paramDeclarations = """
            const params: { [key: string]: any } = {};
            ${queryParams.joinToString(NL) { "params['${it.queryParam}'] = ${it.fieldName};" }}
        """.trimIndent()
        val headerParams = serviceMethod.parameters.filterIsInstance(RestServiceMethodHeaderParameter::class.java)
        val headerDeclarations = """
            const headers: { [key: string]: string } = { } = {};
            headers['Content-Type'] = 'application/json; charset=utf-8';
            ${headerParams.joinToString(NL) { "headers['${it.headerName}'] = ${it.fieldName};" }}
        """.trimIndent()
        var pathDeclaration = "let path = '${combinePaths(parentPath, serviceMethod.path)}';"
        val pathParams = serviceMethod.parameters.filterIsInstance(RestServiceMethodPathParameter::class.java)
        for(pathParam in pathParams) {
            pathDeclaration += "$NL path = path.replace(/${pathParam.pathParam}/g, ${pathParam.fieldName} + \"\");"
        }
        pathDeclaration += "$NL path = this.endpoint() + path.replace(/\\/+/g, '/');"

        val sb = StringBuilder()
        sb.append(
            """
            public ${serviceMethod.methodName}($parameterString): Promise<AxiosResponse<$responseBodyTsDeclaration, any>> {
                $pathDeclaration
                $requestEntityBody
                $paramDeclarations
                $headerDeclarations
                return axios.request<$responseBodyTsDeclaration>({
                    url: path,
                    method: '$method',
                    params: params,
                    headers: headers
                    ${if(bodyParam != null) ",data: jsonData" else ""}
                    ${if(successTransformerResponseBody.isNotEmpty()) ",transformResponse: $successTransformerResponseBody" else ""}
                })
            }

        """.trimIndent()
        )
        return sb.toString()
    }
}